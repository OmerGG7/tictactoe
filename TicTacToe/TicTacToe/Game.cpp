#include "Game.h"

Game::Game()
{
	this->board = new Board();
	this->currentTurn = xTurn;
}

std::string Game::getBoard()
{
	return this->board->getBoardMsg();
}

void Game::putChar(int row, int col)
{
	if (row < 0 || col < 0 || row > 2 || col > 2)
	{
		throw std::exception("Please select a valid row and col.");
	}
	this->board->putCharOnBoard(this->currentTurn, row, col);
	if (this->currentTurn == xTurn)
	{
		this->currentTurn = oTurn;
	}
	else
	{
		this->currentTurn = xTurn;
	}
}

char Game::checkWin()
{
	if (this->board->checkRowOne() != ' ')
	{
		return this->board->checkRowOne();
	}
	else if (this->board->checkRowTwo() != ' ')
	{
		return this->board->checkRowTwo();
	}
	else if (this->board->checkRowThree() != ' ')
	{
		return this->board->checkRowThree();
	}
	else if (this->board->checkColOne() != ' ')
	{
		return this->board->checkColOne();
	}
	else if (this->board->checkColTwo() != ' ')
	{
		return this->board->checkColTwo();
	}
	else if (this->board->checkColThree() != ' ')
	{
		return this->board->checkColThree();
	}
	else if (this->board->checkDiagOne() != ' ')
	{
		return this->board->checkDiagOne();
	}
	else if (this->board->checkDiagTwo() != ' ')
	{
		return this->board->checkDiagTwo();
	}
	else if (this->board->fullBoard())
	{
		return 'd';
	}
	else
	{
		return ' ';
	}
}
