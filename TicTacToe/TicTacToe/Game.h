#pragma once
#include "Board.h"

class Game
{
private:
	Board* board;
	char currentTurn;

public:
	Game();
	std::string getBoard();
	void putChar(int row, int col);
	char checkWin();
};