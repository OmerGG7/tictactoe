#include "Board.h"

Board::Board()
{
	emptyBoard();
}

void Board::emptyBoard()
{
	for (int i = 0; i < rowAndCol; i++)
	{
		for (int j = 0; j < rowAndCol; j++)
		{
			this->gameBoard[i][j] = ' ';
		}
	}
}

bool Board::fullBoard()
{
	for (int i = 0; i < rowAndCol; i++)
	{
		for (int j = 0; j < rowAndCol; j++)
		{
			if (this->gameBoard[i][j] == ' ')
			{
				return false;
			}
		}
	}
	return true;
}

std::string Board::getBoardMsg()
{
	std::string msg = "";
	for (int i = 0; i < rowAndCol; i++)
	{
		for (int j = 0; j < rowAndCol; j++)
		{
			if (this->gameBoard[i][j] == ' ')
			{
				msg += "- ";
			}
			else
			{
				msg += this->gameBoard[i][j];
				msg += " ";
			}
		}
		msg = msg.substr(0, msg.size() - 1);
		msg += '\n';
	}
	return msg;
}

void Board::putCharOnBoard(char turn, int row, int col)
{
	if (this->gameBoard[row][col] != ' ')
	{
		throw std::exception("This square is taken!\nPlease select another square.");
	}
	this->gameBoard[row][col] = turn;
}

char Board::getCharOnBoard(int row, int col)
{
	return this->gameBoard[row][col];
}

char Board::checkRowOne()
{
	if (this->gameBoard[0][0] == this->gameBoard[0][1] && this->gameBoard[0][1] == this->gameBoard[0][2])
	{
		return this->gameBoard[0][0];
	}
	return ' ';
}

char Board::checkRowTwo()
{
	if (this->gameBoard[1][0] == this->gameBoard[1][1] && this->gameBoard[1][1] == this->gameBoard[1][2])
	{
		return this->gameBoard[1][0];
	}
	return ' ';
}

char Board::checkRowThree()
{
	if (this->gameBoard[2][0] == this->gameBoard[2][1] && this->gameBoard[2][1] == this->gameBoard[2][2])
	{
		return this->gameBoard[2][0];
	}
	return ' ';
}

char Board::checkColOne()
{
	if (this->gameBoard[0][0] == this->gameBoard[1][0] && this->gameBoard[1][0] == this->gameBoard[2][0])
	{
		return this->gameBoard[0][0];
	}
	return ' ';
}

char Board::checkColTwo()
{
	if (this->gameBoard[0][1] == this->gameBoard[1][1] && this->gameBoard[1][1] == this->gameBoard[2][1])
	{
		return this->gameBoard[0][1];
	}
	return ' ';
}

char Board::checkColThree()
{
	if (this->gameBoard[0][2] == this->gameBoard[1][2] && this->gameBoard[1][2] == this->gameBoard[2][2])
	{
		return this->gameBoard[0][2];
	}
	return ' ';
}

char Board::checkDiagOne()
{
	if (this->gameBoard[0][0] == this->gameBoard[1][1] && this->gameBoard[1][1] == this->gameBoard[2][2])
	{
		return this->gameBoard[0][0];
	}
	return ' ';
}

char Board::checkDiagTwo()
{
	if (this->gameBoard[0][2] == this->gameBoard[1][1] && this->gameBoard[1][1] == this->gameBoard[2][0])
	{
		return this->gameBoard[0][2];
	}
	return ' ';
}