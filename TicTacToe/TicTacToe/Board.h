#pragma once
#include <iostream>

#define rowAndCol 3
#define xTurn 'X'
#define oTurn 'O'

class Board
{
private:
	char gameBoard[rowAndCol][rowAndCol];

public:
	Board();
	void emptyBoard();
	bool fullBoard();
	std::string getBoardMsg();
	void putCharOnBoard(char turn, int row, int col);
	char getCharOnBoard(int row, int col);
	char checkRowOne();
	char checkRowTwo();
	char checkRowThree();
	char checkColOne();
	char checkColTwo();
	char checkColThree();
	char checkDiagOne();
	char checkDiagTwo();
};